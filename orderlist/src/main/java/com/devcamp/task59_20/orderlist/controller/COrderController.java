package com.devcamp.task59_20.orderlist.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task59_20.orderlist.model.orderList;
import com.devcamp.task59_20.orderlist.repository.OrderRepository;

@RequestMapping("/")
@CrossOrigin
@RestController
public class COrderController {
    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/orders")
    public ResponseEntity<List<orderList>> getAllOrders(){
        try {
            List<orderList> listOrder  = new ArrayList<orderList>();
            orderRepository.findAll().forEach(listOrder::add);
            return new ResponseEntity<>(listOrder, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
