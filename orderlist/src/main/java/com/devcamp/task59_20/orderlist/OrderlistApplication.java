package com.devcamp.task59_20.orderlist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrderlistApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrderlistApplication.class, args);
	}

}
