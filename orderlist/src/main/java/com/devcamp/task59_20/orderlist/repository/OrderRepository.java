package com.devcamp.task59_20.orderlist.repository;

import org.springframework.data.repository.Repository;

import com.devcamp.task59_20.orderlist.model.orderList;

public interface OrderRepository extends Repository<orderList, Long> {

    Iterable<orderList> findAll();
    
}
